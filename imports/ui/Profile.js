import React,  {Component }from 'react'; 

// Task component - represents a single todo item
export default class Profile extends Component {
    render() {
		if (this.props.profiledata.profile_image != undefined) {
			return ( < div class = "profileDiv" >  < div class = "row" >  < img src =  {this.props.profiledata.profile_image}/>  < div class = "stats" >  < h5 > Followers </h5 >  < p >  {this.props.summary.followers} </p >  </div >  < div class = "stats" >  < h5 > Favorites </h5 >  < p >  {this.props.summary.favorites} </p >  </div >  < div class = "stats" >  < h5 > ReTweet Count </h5 >  < p >  {this.props.summary.reTweets_count} </p >  </div >  < div class = "stats" >  < h5 > Status </h5 >  < p >  {this.props.summary.statusCount} </p >  </div >  </div >  < h3 >  {this.props.profiledata.name} </h3 >  </div > ); 
		}
		return( < div ></div > )
    }
}