import React,  {Component }from 'react'; 
import ReactDOM from 'react-dom'; 

import axios from 'axios'
import Tweet from './Tweet.js'; 
import Profile from './Profile.js'; 


// App component - represents the whole app
class App extends Component {
	constructor() {
		super(); 
		this.state =  {
			response: {
				tweets:[ ], 
				userSummary: {}, 
				profileData: {}
			}, 
			username:""
		}; 
	}
	renderTweets() {
		console.log("renderTweets", this.response); 
		return this.state.response.tweets.map((tweet) => ( < Tweet key =  {tweet._id}tweet =  {tweet}/> 
		)); 
	}
	setResponse(twitterResponse) {
		this.setState( {response:twitterResponse}, () =>  {
			console.log("twitterResponse", this.response); 
		})
		// this.response = twitterResponse; 
		// console.log(this.response); 
	}
	handleSubmit(event) {
		event.preventDefault(); 

		// Find the text field via the React ref
		const username = ReactDOM.findDOMNode(this.refs.userName).value.trim(); 
		this.setState( {"username":username}, () =>  {
			console.log("----", this.state.username); 
		})
		
		// this.props.tweets = []; 
		// Meteor.call('tweets.getTweets', username); 


		axios.get('http://localhost:8080/getSummary/' + username)
			.then((responseGetTwets) =>  {
				console.log(responseGetTwets.data)
				this.setResponse(responseGetTwets.data); 
				// this.renderTweets(); 
			})




		// Clear form
		// ReactDOM.findDOMNode(this.refs.userName).value = ''; 
	}
	getMoreTweets(event) {
		const username = ReactDOM.findDOMNode(this.refs.userName).value.trim(); 
		axios.get('http://localhost:8080/getTweets/' + username + "?max_id=" + this.state.response.tweets[this.state.response.tweets.length - 1].id + "&limit="+200)
			.then((responseGetTwets) =>  {
				console.log(responseGetTwets.data)
				// this.setResponse(responseGetTwets.data); 
				this.setState( {response: {
					userSummary:this.state.response.userSummary, 
					profileData:this.state.response.profileData, 
					tweets:this.state.response.tweets.concat(responseGetTwets.data)
				}}, () =>  {
					console.log("twitterResponse", this.state.response); 
				})
				// this.renderTweets(); 
			})
	}

	render() {
		return ( < div className = "container" >  < header >  < h1 > Twitter Feed </h1 >  < form className = "user-name"onSubmit =  {this.handleSubmit.bind(this)} >  < input
							type = "text"
							ref = "userName"
							placeholder = "Find a user on Twitter "
		/>  </form >  </header >  < Profile profiledata =  {this.state.response.profileData}summary =  {this.state.response.userSummary}/>  < ul >  {this.renderTweets()} </ul >  < button onClick =  {this.getMoreTweets.bind(this)}class = "loadTweet" > Load More Tweets </button >  </div > ); 
	}
}

export default App