import { Meteor } from 'meteor/meteor';

import express from 'express';
import tweet from './tweet'



export function setupApi() {
    const app = express();
    app.get('/getTweets/:username', tweet.getTweets);
    app.get('/getSummary/:username', tweet.getSummary);

    WebApp.connectHandlers.use(app);
}