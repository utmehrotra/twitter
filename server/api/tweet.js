import Twit from 'twit';


var T = new Twit({
    "consumer_key": "tqgojehehR4uwMAtC6vyNmYyN",
    "consumer_secret": "A4RZGalAe9z0sgsAGzznqLs7ti3DMet0jqtYbgHBMXJouQwXiU",
    "access_token": "256735487-XM7pR1cmXZ5MX7XLdIQrGbYgOxaHSZL4Ju7bUaOZ",
    "access_token_secret": "z3F9SDhU8qRf6rdQI1Gw7NJzTKMHFjm8wsBp4FWXHZjNM",
    timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
    strictSSL: true, // optional - requires SSL certificates to be valid.
})
let getTwitterTweets = function(user, count, maxId = "") {
    return new Promise((resolve, reject) => {
        var options = {
            screen_name: user,
            count: count
        };
        (maxId !== "") ? options.maxId = maxId: false;
        T.get('statuses/user_timeline', options, function(err, data, response) {
            if (err) {
                return reject(err);
            } else {
                return resolve(data)
            }
        })
    })
};

function translateTweetObject(tmpTweets) {
    OurTweetStr = tmpTweets.map((tweet) => ({ tweet_text: tweet.text, username: tweet.user.screen_name, id: tweet.id, date: tweet.created_at }));
    return OurTweetStr;
}

function saveTweetsInDataBase(tmpTweets, user) {
    var response = Tweets.find({ username: user }, { id_str: 1 }).fetch().map(function(x) {
        return x.id_str;
    })
    OurTweetStr = tmpTweets.filter(value => -1 == response.indexOf(value.id_str));
    console.log("Adding tweets", OurTweetStr.length);
    bulk = Tweets.rawCollection().initializeUnorderedBulkOp();
    OurTweetStr = tmpTweets.map((tweet) => ({ id_str: tweet.id_str, tweet_text: tweet.text, username: tweet.user.screen_name, id: tweet.id, date: tweet.created_at }));
    for (var i = 0, len = OurTweetStr.length; i < len; i++) {
        bulk.insert(OurTweetStr[i]);
    }

    bulk.execute();
    // Tweets.batchInsert(translateTweetObject(OurTweetStr));
}

function getTweets(req, res) {
    if (req.params.username) {
        let user = req.params.username.trim();
        let count = (req.query.limit) ? req.query.limit : 50;
        let maxId = (req.query.max_id) ? req.query.max_id : "";
        let startDate = (req.query.startDate) ? req.query.startDate : false;
        let endDate = (req.query.endDate) ? req.query.endDate : false;

        getTwitterTweets(user, count, maxId)
            .then((tmpTweets) => {
                saveTweetsInDataBase(tmpTweets, user);
                res.status(200).json(translateTweetObject(tmpTweets));
                // res.status(200).json(tmpTweets);
            })
            // .catch((err) => {
            //     res.status(500).json({ err: err, message: 'findFailure' });
            // });
    } else {
        res.status(500).json({ err: { message: 'user required' }, message: 'user required' });
    }

}

function getTimeLine(user) {
    return new Promise((resolve, reject) => {
        let options = {
            screen_name: user
        };
        T.get('users/show', options, function(err, data, response) {
            if (err) {
                return reject(err);
            } else {
                return resolve(data)
            }
        })
    })
}

function getSummary(req, res) {
    if (req.params.username) {
        var username = req.params.username.trim();
        getTimeLine(username).then((profile) => {
            console.log(profile);
            var response = {
                userSummary: {
                    statusCount: profile.statuses_count,
                    lists: profile.listed_count,
                    followers: profile.followers_count,
                    favorites: profile.favourites_count,
                    reTweets_count: profile.status.retweet_count
                },
                tweets: [],
                profileData: {
                    name: profile.name,
                    location: profile.location,
                    description: profile.description,
                    profile_image: profile.profile_image_url_https,
                    createdAt: profile.created_at
                }
            }
            getTwitterTweets(username).then((resp) => {
                response.tweets = translateTweetObject(resp);
                res.status(200).json(response);
            })

        });
    } else {
        res.status(500).json({ err: { message: 'user required' }, message: 'user required' });
    }


}
module.exports = {
    getTweets: getTweets,
    getSummary: getSummary
}